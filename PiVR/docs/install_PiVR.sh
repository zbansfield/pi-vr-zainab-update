#!/bin/bash

# IMPORTANT: Don't open this file on Windows and save again. The reason
# being that line endings are different between DOS/Windows and
# Unix/Linux. DOS uses carriage return and line feed ('\r\n') while
# Unix uses just line feed ('\n').
# If this file is opened and saved in Windows just run this command
# before opening the sh file on Linux
# tr -d '\r' <install_PiVR.sh> outfile
# then rename the 'outfile and delete the original

# check if gitlab.com can be reached, in effect checking if the RPi is
# connected to the internet!
wget -q --spider https://gitlab.com

if [ $? -eq 0 ]; then

    echo "Gitlab.com can be reached...updating now!"

    # update the Raspberry Pi software
    yes Y | sudo apt-get update

    # Go to Home directory
    cd ~/

    # Download PiVR software - as long as it's password protected user will have to manually enter username/password
    git clone https://gitlab.com/louislab/PiVR

    # copy the Shortcut to the Desktop
    cd ~/PiVR/PiVR
    cp Shortcut.desktop ~/Desktop

    # Install all python3 packages that are necessary
    yes Y | sudo apt-get install python3-matplotlib
    yes Y | sudo apt-get install python3-pandas
    yes Y | sudo apt-get install python3-pil.imagetk
    yes Y | sudo apt-get install python3-natsort
    yes Y | sudo apt-get install python3-skimage

    # install imageio - currently not in apt repository
    # Go to Home directory
    cd ~/
    git clone https://github.com/imageio/imageio
    cd imageio
    sudo python3 setup.py install

    # Go to Home directory
    cd ~/
    if grep "start_x=1" /boot/config.txt
    then
        exit
    else
        sudo sh -c "echo 'start_x=1' >> /boot/config.txt"
        reboot
    fi
        exit

else
    echo "PLEASE MAKE SURE YOU ARE CONNECTED TO THE INTERNET!"
    echo "ERROR: https://Gitlab.com could not be reached"

fi

