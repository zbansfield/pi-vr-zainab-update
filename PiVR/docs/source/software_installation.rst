PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _software_installation:

PiVR software installation
==========================

.. _software_installation_RPi:

Install PiVR on the Raspberry Pi
--------------------------------

The easiest way to install PiVR on your Raspberry Pi is to just
follow the :ref:`instructions during hardware construction<PIVR
Hardware Manual>`.

If you just want to download the installation script to your
Raspberry Pi, :download:`press
here <../install_PiVR.sh>`

.. _software_installation_PC:

Install PiVR on a PC
---------------------

#. Install `miniconda <https://docs.conda.io/en/latest/miniconda.html>`__
   on your computer.

#. Install `git <https://git-scm.com/downloads>`__ on your computer

.. note::
   If you have Windows, you may try :ref:`this
   guide<software_install_Win10>` which will install the software
   more or less automatically.

.. note::
   If you have Ubuntu, you may try
   :ref:`this guide<software_install_Ubuntu>` which will install the
   software more or less automatically.

#. Now, create an empty conda environment:

   .. code-block:: python

      conda create --name PiVR_environment

#. Activate the environment you just created by typing:

   Linux/Mac:

   .. code-block:: python

      source activate PiVR_environment

   Windows:

   .. code-block:: python

      activate PiVR_environment

#. Install the a number of packages which are necessary to run the PiVR
   software by copying each line of code into the Terminal

   .. code-block:: python

      conda install -y python=3.7

      conda install -y matplotlib

      conda install -y pandas

      conda install -y scipy

      conda install -y natsort

      conda install -y scikit-image

      conda install -c conda-forge opencv

      Windows/Linux:
      conda install -c conda-forge imageio-ffmpeg

      MacOS:
      conda install -c conda-forge ffmpeg

#. You have now prepared the
   `virtual environment <https://towardsdatascience.com/getting-started-with-python-environments-using-conda-32e9f2779307>`__ PiVR will be running in.

#. Using the anaconda terminal, change the working directory to a
   folder where you want to store the actual PiVR software.

    .. code-block:: python

       cd C:\Users\UserA\Documents>

    .. note::

       You might want to write down the exact path so that you will
       find it again in the future!

#. Download the software by typing:

    .. code-block:: python

       git clone https://gitlab.com/louislab/PiVR

#. Now navigate into the folder you have just downloaded by typing:

   .. code-block:: python

      cd PiVR

#. To start the PiVR software type:

   .. code-block:: python

      python start_GUI.py

.. _software_install_Win10:

Install PiVR on a Windows 10 PC
-------------------------------

.. important::

   If you are having trouble with this installation procedure, do the
   :ref:`manual install<software_installation_PC>`.

.. warning::

   Only Win10, 64bit tested!

#. Open the Anaconda prompt

#. Navigate into a folder where you want to store the PiVR software,
   for example:

   .. code-block:: python

      cd C:\Users\UserA\Documents>

#. Download the software by typing:

   .. code-block:: python

      git clone https://gitlab.com/louislab/PiVR

#. Navigate into the installation folder by typing:

   .. code-block:: python

      cd PiVR\Installation_update

#. Create the Windows 10 virtual environment for the PiVR software to
   run using the provided package list by typing:

   .. code-block:: python

      conda create --name PiVR_environment --file PiVR_Win64.txt

#. Once done, activate the virtual environment by typing:

   .. code-block:: python

      activate PiVR_environment

   You know you successfully activated the virtual enviroment if it
   says '(PiVR)' at the beginnig of the line in the terminal.

#. Start the software by going into the folder where the file
   "start_GUI.py" can be found, which is the parent folder of the
   installation folder you should be in now. So just type:

   .. code-block:: python

      cd ..

#. And to finally start PiVR, type:

   .. code-block:: python

      python start_GUI.py

.. _software_install_Ubuntu:

Install PiVR on a Linux PC
---------------------------

.. important::

   If you are having trouble with this installation procedure, do the
   :ref:`manual install<software_installation_PC>`.

.. warning::

   Only Ubuntu, 64bit tested)

#. Open the Terminal

#. Navigate into a folder where you want to store the PiVR software,
   for example:

   .. code-block:: python

      cd /home/UserA

#. Clone the repository by typing:

   .. code-block:: python

      git clone https://gitlab.com/louislab/PiVR

#. Navigate to the "Installation_update" folder of the repository you
   just cloned:

   .. code-block:: python

      cd /home/UserA/PiVR/PiVR/Installation_update

#. Create the Linux virtual environment for the PiVR software to
   run using the provided package list by typing:

   .. code-block:: python

      conda create --name PiVR_environment --file PiVR_Linux64.txt

#. Once done, activate the virtual environment by typing:

   .. code-block:: python

      source activate PiVR_environment


   You know you successfully activated the virtual enviroment if it
   says '(PiVR)' at the beginnig of the line in the terminal.

#. Start the software by going into the folder where the file
   "start_GUI.py" can be found, which is the parent folder of the
   installation folder you should be in now. So just type:

   .. code-block:: python

      cd ..

#. Start the program by typing:

   .. code-block:: python

      python start_GUI.py


.. _software_start_PC:

Start PiVR on a PC
-------------------

.. note::

   To run PiVR, you of course need to first
   :ref:`install<software_installation_PC>` the software.

#. Open the Anaconda terminal (Windows) or Terminal (MacOS/Linux)

#. Activate the virtual environment you have created during the
   installation. If you followed these instructions type:

   Windows:

   .. code-block:: python

      activate PiVR_environment

   Linux/MacOS:

   .. code-block:: python

      source activate PiVR_environment

#. Change directory to the folder where you downloaded the PiVR
   software into. In the example here we used:

   .. code-block:: python

      cd C:\Users\UserA\Documents\PiVR\PiVR

#. Start PiVR software by typing:

   .. code-block:: python

      python start_GUI.py
