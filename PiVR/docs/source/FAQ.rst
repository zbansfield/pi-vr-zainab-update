PiVR has been developed by David Tadres and Matthieu Louis (`Louis Lab
<https://labs.mcdb.ucsb.edu/louis/matthieu/>`__).

.. _FAQ:

FAQ
===================

.. _FAQ_undistort:

Distorted Images
-----------------

*Question:*
  Why do my images/videos look distorted?

*Answer:*
  Every lens will introduce radial distortions to the image.
  Since the Raspbbery Pi Camera lens is not a high quality lens the
  radial distortion can become very obvious. The distortion is a
  function of the lens you are using meaning the distortion is
  identical for all videos/images taken by the same camera.

  To get remove camera distortions in videos, :ref:`please see
  here<undistort_h264_GUI>`

Progress Bar
-------------

*Question:*
  When running a tracking/virtual reality/video experiment on PiVR, why
  is there no progress bar?

*Answer:*
  PiVR was designed to process each frame as quickly as it can. This is
  necessary to produce realistic virtual realities. Having a progress
  bar undermines this goal as the act of updating the progress bar
  increase latency.

  @ Developers: If you think this problem can be solved, please
  `let us know <https://gitlab.com/LouisLab/pivr/issues>`_ - if there
  is a way to update a progress bar in less than a
  millisecond, this could and should be implemented.

Raspberry Pi does not turn on
-----------------------------

*Question:*
  I am unable to start the Raspberry Pi. It seems to start, but it
  never progresses to the Desktop.

*Answer:*
  We have observed this when the SD card was completely full! This
  can happen if you run experiments until you encounter an error
  saying that the experimental data can not be saved. If you then
  turn off the Raspberry Pi, the OS is unable to start.

  We have been able to recover the data by removing the SD card from
  the PiVR setup and putting into a Ubunut workstation card reader.
  The files on the SD card can then be rescued. After removing the
  files, the Raspberry Pi was able to start again.

  Alternatively, you can also just format the SD card and re-install
  Raspbian and PiVR, of course.

  To avoid this error in the future, please always remove data before
  the SD card fills up!

PiVR won't start a video/experiment
-----------------------------------

*Question:*
  I want to record a video/start and experiment, but nothing happens.

*Answer:*
  There can be several causes for this. Usually, the terminal will be
  very helpful in troubleshooting why you are having a particular
  problem. Here, we list common problems and how to fix them. If you
  do not find your error message listed here, please take a
  picture/write it down and open an `issue <https://gitlab
  .com/LouisLab/pivr/issues>`_

    a) *PermissionError: [Errno 13] Permission denied: ...*
        Please make sure to select a folder where the experiment
        should be saved. That is the button next to "Save in:"
    b) *The camera is already using port %d ' % splitter_port) ...*
        We have seen this error after incorrectly closing the PiVR
        software. Restart the Raspberry Pi to fix this.

PiVR software won't start
--------------------------

*Question:*
  I can't start the PiVR software on the Raspberry Pi

*Answer:*
  There could be several causes for this. To get to the bottom of the
  this bug please do the following:

    a) Restart the Raspberry Pi.
    b) Doubleclick on the desktop shortcut as if starting the software
       (this step is necessary!).
    c) Open the terminal.
    d) Change directory to the PiVR folder and start PiVR manually.
       If you installed PiVR with the provided script you would type::

          cd PiVR/PiVR
          python3 start_GUI.py

    e) See below if your error is explained below. If not, please take a
       picture/write it down and open an
       `issue <https://gitlab.com/LouisLab/pivr/issues>`_.

1) Camera error

.. figure:: Figures/FAQ/1_cam_not_enabled.png
   :alt: 1_cam_not_enabled.png

This could be due to several issues:

  - The camera could not have been enabled. The provided installation
    script normally enables the camera but it worth
    `double-checking <https://projects.raspberrypi.org/en/projects/getting-started-with-picamera/2>`_.

  - If this does not solve the problem, double-check whether the cable
    connecting the Raspberry Pi and the camera is plugged in with the
    correct orientation.

  - If this does not solve the problem it is likely that either the
    camera cable or the camera itself is faulty. It is more likely that
    the cable is faulty as we have experienced this more than once
    already. The solution is to buy a new camera cable and test it.
    If this does not fix the problem, try replacing the camera.
