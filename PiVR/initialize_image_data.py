__author__ = 'David Tadres'
__project__ = 'PiVR'

# Careful: This is a very resource unfriendly way of doing this.


import os
import tkinter as tk
import zipfile
from glob import glob

import imageio
import numpy as np
from natsort import natsorted


def get_self_images():
    """
    Different analyses require the software to look for a variety
    of image data, for example already packed up numpy arrays,
    'raw' h264 video files, well anotated mp4 files or just a
    sequence of images files.
    To keep it relatively easy to maintain, this function is
    introduced.
    By changing the 'video_formats' lists it's easy to add other
    video formats as long as they are supported by the underlying
    ffmpeg reader.
    :return: images - a numpy array containing all the images in the
    following format: [Rows, Columns, Time]
    """
    # What files exist in the folder?
    files = [p.replace('\\', '') for p in glob('*')]
    # Try to guess the framerate
    framerate = None

    images = None
    if 'all_images_undistorted.npy' in files:
        images = np.load('all_images_undistorted.npy')
    elif 'images.npy' in files:
        # todo - get rid sometime in the future - Was necessary
        #  as in this class I've saved images
        # todo - with the name images.npy instead of
        #  all_images.npy. Fixed this on 18/10/16
        images = np.load('images.npy')
    elif 'all_images.npy' in files:
        images = np.load('all_images.npy')
    elif images is None:
        # Next, try to find a video file
        # get a list of all the files in the folder selected by
        # the user

        # Need this as a bool switch in the first elif
        video_object = None
        # If no numpy array could be found, look for videos as
        # defined below
        video_formats = ['h264', 'avi', 'AVI', 'mp4', 'MP4']
        # variable which gets active IF user manually selects a video
        # file
        manual_video_selected = None

        print("Couldn't find a 'all_images.npy' file - looking "
              "for the following video file formats " +
              repr(video_formats))

        files = [p.replace('\\', '') for p in glob('*')]
        # print(files)
        # cycle through all the files in the folder as there's no
        # way of knowing what the name of the video file is
        for i in range(len(files)):
            # only know that it ends with h264
            if files[i].split('.')[-1] in video_formats:
                # if undistort is in filename, assume that this is the
                # video file the user wants to use
                if '_undistorted' in files[i]:
                    video_object = imageio.get_reader(files[i],'ffmpeg')
                    print('Found undistorted video which will be used: ' + files[i])
                    break

                # if there's more than one h264 file in the folder
                # warn the user
                if video_object is not None:
                    print(
                        'More than one video file in folder',
                        'Found more than one file with one of the'
                        'following extensions.\n' +
                        repr(video_formats) + '\nPlease manually '
                        'select the video you want to use for analysis'
                                              )
                    # and ask which video specifically they want to use
                    manual_video_selected = tk.filedialog.askopenfilename(
                                    title='Select video to use',
                                    initialdir=os.getcwd())
                    # and break the loop
                    break

                # if a video file is found get a reader object using imageio
                video_object = imageio.get_reader(files[i], 'ffmpeg')
                print('found video object: ' + files[i])

        if manual_video_selected is not None:
            video_object = imageio.get_reader(manual_video_selected, 'ffmpeg')
            video_file_name = files[i].split('.')[-2]

        if video_object is not None:
            # BUG: When recording from PiVR using the video
            # function something's wrong with the h264 metadata.
            # The 'nframes' parameter is 'inf'
            if np.isinf(video_object.get_meta_data()['nframes']):
                video_length = 0
                # how many frames is the video? That's a bit
                # ridiculous as this is a waste of resources but
                # since the video_object.get_length() parameter gives
                # me 'inf' I don't really know what to do otherwise.
                for i in video_object:
                    video_length += 1
                    # preallocate an empty array with dimension of
                    # rows, columns and then frames
            # Other cameras save the metadata correctly, no need
            # to jump through the above mentioned hoops.
            else:
                video_length = video_object.get_meta_data()[
                    'nframes']
                framerate = video_object.get_meta_data()['fps']

            images = np.zeros(
                (video_object.get_meta_data()['size'][1],
                 video_object.get_meta_data()['size'][0],
                 video_length),
                dtype=np.uint8)

            # put all the frames in a numpy array - this will be
            # hard to do with machines with little RAM for long
            # videos/videos with high framerates!
            for i, im in enumerate(video_object):
                images[:, :, i] = im[:, :, 0]  # only take one
                # channel of the three > grayscale

            # not sure if smart to save this. It's insanely large
            # (e.g. a 5MB video of 60sec at 80fps is easily 1.5Gb
            # in uncompressed format. But here's the switch one
            # could use:

            # np.save('all_images.npy', images)

        elif video_object is None:
            print('Did not find a video file - looking for jpg images')
            # if no video object was found look for single animals

            images_names = [p.replace('\\', '') for p in glob('*' + '.jpg')]
            images_names = natsorted(images_names)
            zf = zipfile.ZipFile('images.zip', mode='w')
            try:
                for i in range(len(images_names)):
                    if i == 0:
                        image_one = imageio.imread(images_names[0])
                        images = np.zeros((image_one.shape[0],
                                                image_one.shape[1],
                                                len(images_names)),
                                               dtype=np.uint8)
                        images[:, :, i] = imageio.imread(images_names[i])[:, :,
                                               0]
                    try:
                        images[:, :, i] = imageio.imread(images_names[i])[:, :,
                                               0]
                    except IndexError:
                        images[:, :, i] = imageio.imread(images_names[i])
                    zf.write(images_names[i])
            finally:
                # close the zip file writing process
                zf.close()

            np.save('all_images.npy', images)

            # After making sure that all the images have been safely stored both in a numpy array AND a zip file, run through
            # them again and remove all of the images
            for i in range(len(images_names)):
                os.remove(images_names[i])

    return(images, framerate)

