*Pick And Place List
*Company=
*Author=
*eMail=
*
*Project=Diagram_v2.2-current_Control
*Date=18:03:52
*CreatedBy=Fritzing 0.9.8b.2021-08-03.CD-92-0-b3d4fefa
*
*
*Coordinates in mm, always center of component
*Origin 0/0=Lower left corner of PCB
*Rotation in degree (0-360, math. pos.)
*
*No;Value;Package;X;Y;Rotation;Side;Name
1;;;76.7997;-51.5573;0;Bottom;Hole1
2;;;16.7727;-50.5483;0;Bottom;Via12
3;;;29.5328;-38.8199;0;Bottom;TXT4
4;;power_jack_slot;12.2419;-6.43173;0;Bottom;12V/13
5;;power_jack_slot;67.6524;-7.84191;0;Bottom;12V IN
6;;;45.3043;-3.54433;0;Bottom;Via23
7;;;29.6884;-37.4947;0;Bottom;TXT3
8;;power_jack_slot;25.7418;-6.43173;0;Bottom;12V/27
9;;;72.2376;-40.6744;0;Bottom;Via3
10;;;49.2983;-35.5696;0;Bottom;Via25
11;;;10.2154;-44.7063;0;Bottom;IMG1
12;;Pinned Version;61.1614;-26.4253;0;Bottom;MiniPuck17
13;;;21.2739;-44.5673;0;Bottom;Via13
14;;Pinned Version;36.6614;-26.4253;0;Bottom;MiniPuck27
15;;;76.8268;-8.08105;0;Bottom;Via20
16;;;74.3054;-20.0436;0;Bottom;Via22
17;;;57.7765;-37.0448;0;Bottom;Via14
18;;;29.539;-40.3659;0;Bottom;TXT2
19;;;74.3054;-7.06307;0;Bottom;Via21
20;;;10.0584;-8.97513;0;Bottom;Via9
21;;;24.2875;-20.0622;0;Bottom;Via26
22;;;29.3496;-42.2516;0;Bottom;TXT1
23;;;38.8284;-38.5988;0;Bottom;Via15
24;;Pinned Version;12.1613;-25.4253;0;Bottom;MiniPuck13
25;;;31.3244;-12.554;0;Bottom;Via17
26;;power_jack_slot;39.2419;-6.43173;0;Bottom;12V/17
27;;;3.29953;-3.05727;0;Bottom;Hole3
28;;;49.2983;-38.5743;0;Bottom;Via24
29;;power_jack_slot;52.7417;-6.43173;0;Bottom;12V/18
30;;;76.7997;-3.05727;0;Bottom;Hole2
31;;THT;75.2995;-40.5573;0;Bottom;+LCD 5V-
32;;;76.8268;-26.0806;0;Bottom;Via19
33;;;52.3028;-15.563;0;Bottom;Via18
34;;;11.8872;-13.8519;0;Bottom;Via10
35;;;36.8808;-8.97513;0;Bottom;Via4
36;;;3.29953;-51.5573;0;Bottom;Hole4
37;;TO220 [THT];60.2596;-42.0813;0;Bottom;GPIO18
38;;THT;43.8092;-50.3272;180;Bottom;RPi GPIO
39;10k;THT;43.3796;-41.0573;0;Bottom;R18
40;;;21.0312;-33.3592;0;Bottom;Via11
