# 3D Builder files

These 3mf files can be opened with the programm "3D Builder". It 
ships with newer Windows distributions (e.g. Windows 10).

If you don't have access to 3D Builder please use the STL files in 
the other folder.