import numpy as np
from skimage.transform import rotate
from scipy.ndimage.interpolation import affine_transform
import matplotlib.pyplot as plt

name_of_file = 'C:\\Users\\tadres\\Dropbox\\PhD\\Programming\\Python\\Git\\SmAl-VR\\SmAl_VR\\VR_arenas' \
               '\\640x480_test_animal_pos[400,150].csv'

arena = np.genfromtxt(name_of_file, delimiter=',')

animal_orientation_in_radians = -np.pi/2
animal_placement_x = 400
animal_placement_y = 150

animal_is_x = 370
animal_is_y = 276


diff_x =  animal_placement_x - animal_is_x
diff_y = animal_placement_y -animal_is_y

#arena = arena[0:arena.shape[0] + diff_y, 0:arena.shape[1] + diff_x]
arena = affine_transform(arena, np.array(([1,0],[0,1])), offset=[diff_y, diff_x])

angle = np.degrees(animal_orientation_in_radians)
degrees = (angle + 360) % 360

# i think i have to use resize == True!
arena = rotate(arena, degrees, mode = 'constant')

plt.imshow(arena)
plt.xlim(0,640)
plt.ylim(480, 0)
plt.grid()

'''
'''
import scipy
from numpy import array, cos, sin, pi, diag, dot, float32
from matplotlib.pyplot import subplot, axis, imshow, cm, show

src=scipy.misc.ascent()
c_in=0.5*array(src.shape)
c_out=array((256.0,256.0))
for i in range(0,7):
    a=i*15.0*pi/180.0
    transform=array([[cos(a),-sin(a)],[sin(a),cos(a)]])
    print(repr(i) + ' transform: ' + repr(transform))
    offset=c_in-c_out.dot(transform)
    # NOTE: cval is for points outside the boundary, default is 0 - could be used to set a minimum light inent
    dst=scipy.ndimage.interpolation.affine_transform(
        src,transform.T,offset=offset, cval=0.0,output=float32
    )
    subplot(1,7,i+1);axis('off');imshow(dst,cmap=cm.gray)
show()

"""
"""

import numpy as np
from scipy import ndimage as ndi
from skimage import data
from scipy import misc
import matplotlib.pyplot as plt

camera = misc.ascent()
θ = np.deg2rad(30)
C = np.cos
S = np.sin

T0 = np.array([[1, 0, -256], [0, 1, -256], [0, 0, 1.]])
Rot = np.array([[C(θ), -S(θ), 0], [S(θ), C(θ), 0], [0, 0, 1]])
T1 = np.array([[1, 0, 256], [0, 1, 256], [0, 0, 1.]])

T_final = np.array([[1,0,0], [0,1,100],[0,0,1.]])

Forward = T_final@ T1 @ Rot @ T0
Inverse = np.linalg.inv(Forward)

camera_rot = ndi.affine_transform(camera, Forward[:2, :2], Forward[:2, 2], order=1)
#camera_trans = ndi.affine_transform(camera_rot, matrix=np.array(([1,0],[0,1])), offset=[0,0])

plt.figure();plt.imshow(camera_rot)