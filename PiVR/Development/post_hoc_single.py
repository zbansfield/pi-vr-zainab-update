__author__ = 'David Tadres'
__project__ = 'PiVR'

"""
RETIRED - now mostly implemented in pre_experiment.py and old_fast_tracking.py. I'll keep it for now as it has some nice
display options we might want to poach in the future
IF the user recorded a single larvae using full frame images this is the script that is best in identifying the 
trajectory of the animal
"""

from skimage.measure import regionprops, label
import numpy as np
import pickle
from scipy import ndimage, misc
import operator
import tkinter as tk
from PIL import Image, ImageTk
from tkinter import messagebox
from skimage.transform import resize
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from skimage import morphology
import skimage
from skimage.draw import line
import time

import tracking_help_classes as thc

# Currently the newest version I can easily put on the Pi using Raspian is 0.12 - the thin function was only introduced
# in version 13. In order to keep it simple just use the backup function if user is using 0.12, but print a statement
if int(skimage.__version__[2:4]) >= 13:
    THINING_FUNCTION = 'Thin'
else:
    THINING_FUNCTION = 'skeletonize'
    print('Skimage version: ' + skimage.__version__ + '! In order to use better skeletonize function update to at least'
                                                      'version 0.13.0')

################################
# todo make dynamic!
#boxsize=50
frames_to_define_orientation = 5
###############################

class PostHocTracking():
    def __init__(self, all_images, timestamps, model_organism, preview, preview_resize, recording_framerate,
                 display_framerate, signal, pixel_per_mm, boxsize=None, organisms_and_heuristics=None):

        self.preview = preview
        if preview == 'debug':
            self.debug_mode = True
            self.resize = 1
        else:
            self.debug_mode = False

        self.preview_resize = preview_resize

        self.all_images = all_images
        # If experiment was run using smAL-VR software the experiment should have a numpy array called 'timestamps'
        # which save the actual time the image was taken. If the experiment does not have this a placeholder will
        # be put for now - just all zeros
        if timestamps is not None:
            self.timestamps = timestamps
        else:
            self.timestamps = np.zeros((all_images.shape[2], 2))

        self.model_organism = model_organism

        self.recording_framerate = recording_framerate
        self.display_framerate = display_framerate
        # print('framerate = ' + repr(self.display_framerate)) # todo
        self.interval = (1 / self.display_framerate) * 1000

        self.organisms_and_heuristics = organisms_and_heuristics

        self.pixel_per_mm = pixel_per_mm
        self.frames_to_define_orientation = frames_to_define_orientation
        self.boxsize = boxsize
        self.repair_ht_swaps = True
        self.size_of_scatterplot = 2
        self.vr_arena = None

        # debug mode resize
        self.resize = 2

        self.signal = signal
        if self.signal == 'white':
            self.compare = operator.gt
            self.box_intensity = 255
        elif self.signal == 'dark':
            self.compare = operator.lt
            self.box_intensity = 0
        else:
            print('signal has to be either "bright" or "dark". Please adjust code.')
            import sys
            sys.exit()

        # this is for the case that the user is testing a video/full frame recording to get all the heuristic rules
        # to track an animal in the future
        # for each frame save:
        # 0) the filled area in pixel
        # 1) the filled area divided by pixel/mm
        # 2) the major over minor axis
        # 3) the eccentricty
        # 4) the length - this is going to be very useful for the boxsize and the searchboxsize
        # 5) length of skeleton divided by pixel/mm
        # 6) speed in pixel per frame
        # 7) speed in mm/frame (just #5 divided by pixel per frame)
        # 8) speed in mm/s (7 * frames per second)
        self.heuristic_parameters = np.zeros((9, all_images.shape[2]))

        # by definition the tracking will always start at zero
        self.start_frame = 0
        self.end_frame = all_images.shape[2]-1

        self.search_boxes = np.zeros((self.all_images.shape[2]+1, 4),
                                dtype=np.int16)  # must be unsigned, otherwise if goes below 0 goes to 32K!

        # pre-allocation of the empty array - might be good to let user decide if skel needs to be pre-allocated
        # For my experiments memory will never be an issue but if someone want to track a fast animal for a long time
        # memory might run out. In numpy even the bool arrays are 8 bit (and not 1bit). For example a boxsize of 25
        # and 9000 frames (9000/30fps=5minutes) the preallocation for EACH array is 22.5Mb. If someone where to run
        # and experiment for one hour (3600 seconds * 30fps = 108000 frames) Each array would be 270Mb. That will
        # probably lead to memory error.
        # Todo in the future we must implement bitarray (or similar) which will allow true 1Bit arrays for the binary arrays
        # OR: Don't save images and only save centroid/head for this kind of long experiment.
        size_of_expected_animals = organisms_and_heuristics[model_organism]['max_skeleton_length_mm']*pixel_per_mm
        self.image_raw = np.zeros((int(np.ceil(size_of_expected_animals * 2)),
                                   int(np.ceil(size_of_expected_animals * 2)),
                                   self.all_images.shape[2]), dtype=np.uint8)

        self.image_thresh = np.zeros((int(np.ceil(size_of_expected_animals * 2)),
                                      int(np.ceil(size_of_expected_animals * 2)),
                                      self.all_images.shape[2]), dtype=np.bool_)
        self.image_skel = np.zeros((int(np.ceil(size_of_expected_animals * 2)),
                                    int(np.ceil(size_of_expected_animals * 2)),
                                    self.all_images.shape[2]), dtype=np.bool_)


        self.bounding_boxes = np.zeros((4, self.all_images.shape[2]), dtype=np.uint16)

        self.centroids = np.zeros((self.all_images.shape[2], 2), dtype=np.int16)

        self.tails = np.zeros((self.all_images.shape[2], 2), dtype=np.int16)
        self.heads = np.zeros((self.all_images.shape[2], 2), dtype=np.int16)
        self.endpoints = np.zeros((2, 2, self.all_images.shape[2]), dtype=int)
        self.ht_swap = np.zeros((self.all_images.shape[2]), dtype=np.bool_)

        # we need this otherwise we can't save the pandas frame
        self.stimulation = np.zeros((self.all_images.shape[2]),dtype=np.float)

        self.length_skeleton = np.zeros((self.search_boxes.shape[0]), dtype=np.uint8)

        self.animal_lost = np.zeros((self.search_boxes.shape[0]), dtype=np.bool_)

        self.organisms_and_heuristics = organisms_and_heuristics
        self.filled_area_min = pixel_per_mm * organisms_and_heuristics[model_organism]['filled_area_min_mm']
        self.filled_area_max = pixel_per_mm * organisms_and_heuristics[model_organism]['filled_area_max_mm']
        self.eccentricity_min = organisms_and_heuristics[model_organism]['eccentricity_min']
        self.eccentricity_max = organisms_and_heuristics[model_organism]['eccentricity_max']
        self.major_over_minor_axis_min = organisms_and_heuristics[model_organism]['major_over_minor_axis_min']
        self.major_over_minor_axis_max = organisms_and_heuristics[model_organism]['major_over_minor_axis_max']
        print('filled_area_min ' + repr(self.filled_area_min))
        print('filled_area_max' + repr(self.filled_area_max))
        print('eccentricity_min' + repr(self.eccentricity_min))
        print('eccentricity_max' + repr(self.eccentricity_max))
        print('major_over_minor_axis_min' + repr(self.major_over_minor_axis_min))
        print('major_over_minor_axis_max' + repr(self.major_over_minor_axis_max))

        self.find_roi_post_hoc()
        self.define_animal()
        self.tracking()

    def find_roi_post_hoc(self):
        """
        This is the function that is called when the experiment has already been done and the user wants to identify
        the position and other characteristics of the model organism used.
        It first reads all the images (user should provide which file format the images are in) and zips them up so
        that the folder gets easier to copy around. It also creates a numpy array with all the images for this script
        to use.

        :return:
        """
        if self.debug_mode:
            child = tk.Toplevel()
            child.grab_set()
            child.wm_title('Looking for a movement')

            child_frame = tk.Frame(child, width=640, height=480)
            child_frame.pack()
            child_canvas = tk.Canvas(child, width=640, height=480)
            child_canvas.place(x=0, y=0)

        # try:
        self.identification_images = None
        self.roi_found = False
        self.counter = 0


        # take the mean image (everything that moves during the experiment disappears
        self.mean_image = np.nanmean(self.all_images, axis=2)

        # save it as in the folder
        misc.imsave('Background.jpg', self.mean_image)

        while self.roi_found == False:
            print('counter ' + repr(self.counter))

            blobs = []

            # keep a reference for the current image
            self.first_image = self.all_images[:, :, self.counter]
            # subtract the current image from the mean image
            first_subtracted_image = self.mean_image.astype(np.int16) - self.all_images[:, :, self.counter].astype(np.int16)
            # calculate the standard deviation of the subtracted image over all pixels.
            std = np.std(first_subtracted_image)
            # due to camera noise we unfortunately have to "smear" the image a bit, we do that with this
            # gaussian filter
            smoothed_image = ndimage.filters.gaussian_filter(first_subtracted_image, sigma=std)

            # now we should have a gaussian distribution of pixel intensity. we just take mean of the
            # smoothened image - a given factor * the standard deviation subtracted, unsmoothened image
            if self.signal == 'white':
                self.overall_threshold = np.nanmean(smoothed_image) - 2 * std
                thresh_image = smoothed_image < self.overall_threshold
            else:
                self.overall_threshold = np.nanmean(smoothed_image) + 2 * std
                thresh_image = smoothed_image > self.overall_threshold

            # regionprops will be used throughout the tracking software:
            # http://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.regionprops
            props = regionprops(label(thresh_image))
            # in case we have more than one regionproperty
            if len(props) > 1:
                # go through all of them
                for i_props in range(len(props)):
                    if self.debug_mode:
                        try:
                            print('in find ROI prop#' + repr(i_props) + ' : Filled Area: ' +
                                  repr(props[i_props].filled_area) + ' eccentricity: ' +
                                  repr(props[i_props].eccentricity)[0:5] + ' major over minor: ' +
                                  repr(props[i_props].major_axis_length / props[i_props].minor_axis_length)[0:5])
                        except ZeroDivisionError:
                            pass

                    # 3 rules at the moment: 1) the are must be in a certain range. 2) The eccentrecity
                    # (='roundness') must be in a certain range (e.g. it shouldn't be a circle) and
                    # 3) the major axis / minor axis must be in a certain range
                    # maybe can kick out the eccentricity? Major/minor is more intuitive.
                    if self.filled_area_min < props[i_props].filled_area < self.filled_area_max and \
                            self.eccentricity_min < props[i_props].eccentricity < self.eccentricity_max and \
                            self.major_over_minor_axis_min < \
                            props[i_props].major_axis_length / props[i_props].minor_axis_length < \
                            self.major_over_minor_axis_max:
                        blobs.append(i_props)
                        if self.debug_mode:
                            print('Interesting blob in prop#' + repr(i_props) + ' : Filled Area: ' +
                                  repr(props[i_props].filled_area) + ' eccentricity: ' +
                                  repr(props[i_props].eccentricity)[0:5] + ' major over minor: ' +
                                  repr(props[i_props].major_axis_length / props[i_props].minor_axis_length)[0:5])

                # if there's only one regionprop, perfect, found the moving object
                if len(blobs) == 1:
                    self.first_roi = thc.FindROI(props[blobs[0]], self.boxsize, 2, self.first_image)
                    self.roi_found = True
                    # Temporarily  save these parameters for display in messagebox if debug mode is on
                    filled_area = props[blobs[0]].filled_area
                    eccentricity = props[blobs[0]].eccentricity
                    major_over_minor = props[blobs[0]].major_axis_length / props[blobs[0]].minor_axis_length
                    print('only one blob at x:' + repr(props[blobs[0]].centroid[1]) + ' y: ' +
                          repr(props[blobs[0]].centroid[0]))
                # otherwise the animal hasn't moved enough yet, or too many things moved,
                # so try again with another picture
                else:
                    pass

            # if there's only one regionprop, and if it's bigger than a defined minimum, perfect, found the animal
            elif len(props) == 1:
                if props[0].area > self.filled_area_min:
                    # If there's only one blob and it makes the cut for  minimal size, try to take the object and say this
                    # is the animal we're looking for
                    self.first_roi = thc.FindROI(props[0], self.boxsize, 2, self.first_image)

                    # Temporarily  save these parameters for display in messagebox if debug mode is on
                    filled_area = props[0].filled_area
                    eccentricity = props[0].eccentricity
                    major_over_minor = props[0].major_axis_length / props[0].minor_axis_length

                    self.roi_found = True
                    print('only one prop')
            # otherwise try again next picture
            elif len(props) == 0:
                pass

            if self.debug_mode and self.counter > 0:
                converted_binary = thresh_image.astype(np.uint8)
                converted_binary[np.where(converted_binary == 1)] = 255
                photo = ImageTk.PhotoImage(image=Image.fromarray(converted_binary))
                child_canvas.create_image(0, 0, image=photo, anchor=tk.NW)
                child.update()

            self.counter += 1

            if self.roi_found:
                if self.debug_mode:
                    stop_searching_roi = messagebox.askyesno('smAL-VR information',
                                                             'The algorithm found something that is moving!\n'
                                                             'If that does not look at all like your animal\n'
                                                             'press "No", to continue press "Yes"\n'
                                                             'Filled Area: ' + repr(filled_area)[0:5] + '\n'
                                                             'Eccentricity: ' + repr(eccentricity)[0:5] + '\n'
                                                             'Major over minor axis: ' + repr(major_over_minor)[0:5])
                    print(stop_searching_roi)
                    if not stop_searching_roi:
                        self.roi_found = False

        if self.debug_mode:
            child.grab_release()
            child.destroy()

            # except:
            #    print('Unable to locate animal. Please restart. If you keep seeing this error message, you have to change '
            #          'your settings or your setup.')
            #    import sys
            #    sys.exit()

        print('Animal found.')

    def define_animal_post_hoc(self):
        """
        # With the information where to look we identify the larva using local thresholding (we couldn't do that before
        # with the whole image)
        # This is only saved if the animal is somewhere where the background illumination is relatively even and the
        # animal stands out clearly relative to it's immediate background!
        """

        # subtract the current image from the mean image
        first_subtracted_image = self.mean_image.astype(np.int16) - self.all_images[:, :, self.counter].astype(
            np.int16)
        # calculate the standard deviation of the subtracted image over all pixels.
        std = np.std(first_subtracted_image)

        smoothed_image = ndimage.filters.gaussian_filter(first_subtracted_image, sigma=std)

        # now we should have a gaussian distribution of pixel intensity. we just take mean of the
        # smoothened image - a given factor * the standard deviation subtracted, unsmoothened image
        if self.signal == 'white':
            self.first_image_thresholded = smoothed_image < self.overall_threshold
        else:
            self.first_image_thresholded = smoothed_image > self.overall_threshold


        # Call the binary image and identify connected regions only in the defined roi
        animal_properties = regionprops(
            label(thc.CallImageROI(self.first_image_thresholded, self.first_roi).small_image))

        if self.debug_mode:
            print(len(animal_properties))
            for special_counter in range(len(animal_properties)):
                try:
                    print('in define animal: ' + repr(special_counter) + ' Filled Area: ' +
                          repr(animal_properties[special_counter].filled_area) + ' eccentricity: ' +
                          repr(animal_properties[special_counter].eccentricity)[0:5] + ' major over minor: ' +
                          repr(animal_properties[special_counter].major_axis_length /
                               animal_properties[special_counter].minor_axis_length)[0:5])
                except ZeroDivisionError:
                    print(repr(special_counter) + ' minor axis was zero')
                    pass

        # Check all the connected regions and define the largest object as the animal
        self.first_animal = thc.DescribeLargestObject(animal_properties, self.first_roi, animal_like=True,
                                                      filled_area_min=self.filled_area_min,
                                                      filled_area_max=self.filled_area_max,
                                                      eccentricity_min=self.eccentricity_min,
                                                      eccentricity_max=self.eccentricity_max,
                                                      major_over_minor_axis_min=self.major_over_minor_axis_min,
                                                      major_over_minor_axis_max=self.major_over_minor_axis_max)



        self.first_row_min = self.first_animal.row_min
        self.first_row_max = self.first_animal.row_max
        self.first_col_min = self.first_animal.col_min
        self.first_col_max = self.first_animal.col_max

        first_frame_data = {'filled area': self.first_animal.filled_area,
                            'centroid row': self.first_animal.centroid_row,
                            'centroid col': self.first_animal.centroid_col,
                            'bounding box row min': self.first_animal.row_min,
                            'bounding box row max': self.first_animal.row_max,
                            'bounding box col min': self.first_animal.col_min,
                            'bounding box col max': self.first_animal.col_max}

        # save the first frame data
        with open('first_frame_data.pkl', 'wb') as handle:
            pickle.dump(first_frame_data, handle, protocol=pickle.HIGHEST_PROTOCOL)

        if self.debug_mode:
            binary_image_to_plot = thc.CallImageROI(self.first_image_thresholded, self.first_roi).small_image

            child = tk.Toplevel()
            child.grab_set()
            child.wm_title('Largest object will be defined as the animal')

            child_frame = tk.Frame(child, width=(binary_image_to_plot.shape[1] * self.resize) * 2,
                                   height=binary_image_to_plot.shape[0] * self.resize)
            # child_frame.pack()
            label_left = tk.Label(child, text='Bounding Box around animal')
            label_left.grid(row=0, column=0)

            child_canvas_left = tk.Canvas(child, width=binary_image_to_plot.shape[1] * self.resize,
                                          height=binary_image_to_plot.shape[0] * self.resize)
            child_canvas_left.grid(row=1, column=0)

            label_right = tk.Label(child, text='Binary image - largest object \n will be defined as the animal')
            label_right.grid(row=0, column=1)
            child_canvas_right = tk.Canvas(child, width=binary_image_to_plot.shape[1] * self.resize,
                                           height=binary_image_to_plot.shape[0] * self.resize)
            child_canvas_right.grid(row=1, column=1)

            # here the identified bounding box is drawn around the animal
            image_object = thc.DrawBoundingBox(self.first_image, self.first_animal, value=self.box_intensity)
            image_to_plot = thc.CallImageROI(image_object.image_with_box, self.first_roi).small_image
            image_to_plot = resize(image_to_plot, (int(image_to_plot.shape[0] * self.resize),
                                                   int(image_to_plot.shape[1] * self.resize)),
                                   preserve_range=True, mode='reflect')
            photo_bounding_box = ImageTk.PhotoImage(image=Image.fromarray(image_to_plot))
            child_canvas_left.create_image(0, 0, image=photo_bounding_box, anchor=tk.NW)

            converted_binary = binary_image_to_plot.astype(np.uint8)
            converted_binary[np.where(converted_binary == 1)] = 255
            converted_binary = resize(converted_binary, (int(converted_binary.shape[0] * self.resize),
                                                         int(converted_binary.shape[1] * self.resize)),
                                      preserve_range=True, mode='reflect'
                                      )
            photo_binary = ImageTk.PhotoImage(image=Image.fromarray(converted_binary))
            child_canvas_right.create_image(0, 0, image=photo_binary, anchor=tk.NW)
            child.update()

            # The messagebox blocks th execution of the code until the user click's ok
            messagebox.showinfo('SmAl-VR information', 'The first image using local thresholding!'
                                                       '\n The algorithm will use the largest object as the animal'
                                                       '\n please press "OK" to continue')

            child.grab_release()
            child.destroy()

        print('Animal defined')
        print('counter ' + repr(self.counter))

    def tracking(self):

        self.child = tk.Toplevel()
        self.child.grab_set()

        if self.debug_mode:
            # if debug mode, turn off the preview - todo to be discussed!
            self.preview = 'none'

            # todo make this a controllable parameter
            self.child.wm_title('Debug Window')

            self.child_frame = tk.Frame(self.child, width=self.boxsize * 2 * self.resize * 2 * 3,
                                        height=self.boxsize * 2 * self.resize * 2 * 2)

            # Display the time that is remaining
            self.time_remaining = tk.Label(self.child, text='')
            self.time_remaining.grid(row=0, column=0)

            self.child_canvas_top_left = tk.Canvas(self.child, width=self.boxsize * 2 * self.resize * 2,
                                                   height=self.boxsize * 2 * self.resize * 2)
            self.child_canvas_top_left.grid(row=1, column=0)

            self.child_canvas_top_middle = tk.Canvas(self.child, width=self.boxsize * 2 * self.resize * 2,
                                                     height=self.boxsize * 2 * self.resize * 2)
            self.child_canvas_top_middle.grid(row=1, column=1)

            self.child_canvas_top_right = tk.Canvas(self.child, width=self.boxsize * 2 * self.resize * 2,
                                                    height=self.boxsize * 2 * self.resize * 2)
            self.child_canvas_top_right.grid(row=1, column=2)
        elif not self.debug_mode:
            self.small_image_resize = self.preview_resize

            self.child.wm_title('Visualization of the program')

            # Show the time that is remaining until the window closes
            self.time_remaining = tk.Label(self.child, text='')

            if self.preview == 'overview':
                # locate the time on the frame
                self.time_remaining.grid(row=0, column=3)

                # self.child_frame = tk.Frame(self.child, width=int(self.smoothed_background.shape[1] / 2 +
                #                                                  (self.boxsize * self.small_image_resize * 2) * 3),
                #                            height=self.smoothed_background.shape[0] / 2)

                # call the plotting library and plot the empty arena into a figure

                self.child_canvas = tk.Canvas(self.child,
                                              width = self.mean_image.shape[1] / self.size_of_scatterplot,
                                              height=self.mean_image.shape[0] / self.size_of_scatterplot)
                self.child_canvas.grid(row=0, column=0, columnspan=3, rowspan=3)

                self.fig = Figure()
                self.ax_vr_arena = self.fig.add_subplot(111)
                # self.image_of_arena = self.ax_vr_arena.imshow(self.vr_arena,
                #                                              vmin=0, vmax=65535)
                # turn off the labels and axis to save space
                # self.ax_vr_arena.axes.get_xaxis().set_ticks([])
                # self.ax_vr_arena.axes.get_yaxis().set_ticks([])

                # bind the plot to the GUI
                self.canvas = FigureCanvasTkAgg(self.fig, master=self.child_canvas)
                self.canvas.show()

                # Add the toolbar
                toolbar = NavigationToolbar2TkAgg(self.canvas, self.child_canvas)
                toolbar.update()
                # have to pack as NavigationToolbar does only accept pack and not grid.
                self.canvas.get_tk_widget().pack()

                self.child_local_canvas = tk.Canvas(self.child,
                                                    width=int(self.boxsize * self.small_image_resize ** 2),
                                                    height=int(self.boxsize * self.small_image_resize ** 2))
                self.child_local_canvas.grid(row=1, column=3)

                self.child_binary_canvas = tk.Canvas(self.child,
                                                     width=int(self.boxsize * self.small_image_resize ** 2),
                                                     height=int(self.boxsize * self.small_image_resize ** 2))
                self.child_binary_canvas.grid(row=1, column=4)

                self.child_skeleton_canvas = tk.Canvas(self.child,
                                                       width=int(self.boxsize * self.small_image_resize ** 2),
                                                       height=int(self.boxsize * self.small_image_resize ** 2))
                self.child_skeleton_canvas.grid(row=2, column=3)

            elif self.preview == 'only animal':
                self.time_remaining.grid(row=0, column=0)

                # self.child_frame = tk.Frame(self.child, width=int(self.boxsize * self.small_image_resize * 4),
                #                            height=int(self.boxsize * self.small_image_resize * 4))
                # self.child_frame
                #if self.Drosophila_larva:
                self.child_text = tk.Label(self.child, text='Green is the head \n Red is the centroid')
                #else:
                #    self.child_text = tk.Label(self.child, text='Red is the centroid')
                self.child_text.grid(column=0, row=1)

                self.child_canvas = tk.Canvas(self.child, width=int(self.boxsize * self.small_image_resize * 4),
                                              height=int(self.boxsize * self.small_image_resize * 4))
                self.child_canvas.grid(column=0, row=2)

            else:
                self.time_remaining.grid(row=0, column=0)
                # self.child_frame = tk.Frame(self.child, width=10, height=10)

                # self.child_canvas = tk.Canvas(self.child,width=10, height=10)
                # self.child_canvas.grid(row=0,column=0)
            # the term below is for saving the data after the experiment has run - has to be called before the actual
            # experiment with a delay which takes the framerate and the time the user wants to record

        self.search_boxes[0, :] = [int(np.round(self.first_row_min - self.boxsize)),
                              int(np.round(self.first_row_max + self.boxsize)),
                              int(np.round(self.first_col_min - self.boxsize)),
                              int(np.round(self.first_col_max + self.boxsize))]
        self.to_fix = []

        number_of_frames_left = self.all_images.shape[2] - self.counter



        # a variable that needs asignment here:
        first_frame_no_animal=False

        #try:

        for self.i_tracking in range(0, number_of_frames_left):
                start_time = time.time()


                print('working on frame ' + repr(self.i_tracking))
                self.current_frame = self.all_images[:,:,self.counter+self.i_tracking]
                # on the edge - this becomes necessary when the animal is close to the boundary of the frame. Due to the
                # boxsize it can happen that the animal is still well in the frame, but the boxsize ask the program to
                # look for the larva outside of the frame - which will lead to an error
                if self.search_boxes[self.i_tracking, 0] < 0:
                    self.search_boxes[self.i_tracking, 0] = 0
                if self.search_boxes[self.i_tracking, 2] > self.all_images.shape[0]:
                    self.search_boxes[self.i_tracking, 2] = self.all_images.shape[0] - 1
                if self.search_boxes[self.i_tracking, 1] < 0:
                    self.search_boxes[self.i_tracking, 1] = 0
                if self.search_boxes[self.i_tracking, 3] > self.all_images.shape[1]:
                    self.search_boxes[self.i_tracking, 3] = self.all_images.shape[1]

                # filter the image to get rid of camera noise. Only take the search box
                smoothed_current_local_image = ndimage.filters.gaussian_filter(
                    thc.CallBoundingBox(self.current_frame, self.search_boxes[self.i_tracking, :]).sliced_image,
                    sigma=1)

                # take only the slice from the backgroudn image that is necessary to compare
                smoothed_background_local_image = \
                    thc.CallBoundingBox(self.mean_image, self.search_boxes[self.i_tracking, :]).sliced_image

                # We have to change the datatype of the numpy array from originally unsigned int 8 (goes from 0 to 255)
                # to signed int 16 (goes from -32768 to 32767). The reason being that if we subtract a two uint8 pictures
                # in case we have 200-201 = 255 while 200 - 199 = 1. This leads the histogram of intensites to have 2
                # background peaks, one around 0 and the other around 255. In int16 space, on the other hand, we'll have
                # the background mean at around 0 while the animal will be in the negative range if it is dark in white
                # background
                subtracted_current_frame = smoothed_current_local_image.astype(
                    np.int16) - smoothed_background_local_image.astype(np.int16)

                # calculate the local threshold
                current_thresh = thc.MeanThresh(subtracted_current_frame, self.signal, 3)

                if len(regionprops(label(subtracted_current_frame > current_thresh.thresh))) == 0:
                    # In case there are zero connected pixels, we assume that's because the 3 STDs are too much and go with
                    # only two # todo, really? Only see the invert=True as different!
                    current_thresh = thc.MeanThresh(subtracted_current_frame, self.signal, 3, invert=True)

                # calculate the binary local image
                current_image_thresholded = self.compare(subtracted_current_frame, current_thresh.thresh)

                # use the regionprops function to identify blobs and characterize them
                # http://scikit-image.org/docs/dev/api/skimage.measure.html#skimage.measure.regionprops
                animal_properties_current = regionprops(label(current_image_thresholded))

                # find the largest blob in the search image and set it as the animal
                current_animal = thc.DescribeLargestObject(animal_properties_current,
                                                           self.search_boxes[self.i_tracking, 0::2])

                # use the bounding box of the largest blob (defined as animal) plus a given boxsize as the next search box
                self.search_boxes[self.i_tracking + 1, :] = [int(np.round(current_animal.row_min - self.boxsize)),
                                                             int(np.round(current_animal.row_max + self.boxsize)),
                                                             int(np.round(current_animal.col_min - self.boxsize)),
                                                             int(np.round(current_animal.col_max + self.boxsize))]

                # get the current width and height of the bounding box
                current_width = current_animal.row_max - current_animal.row_min
                current_height = current_animal.col_max - current_animal.col_min

                if current_width > self.image_raw.shape[0] or current_height > self.image_raw.shape[1]:
                    tk.messagebox.showerror('Blob too large',
                                            'The expected size of the animal is no larger than: ' +
                                            repr(self.image_raw.shape[0]) + ' x ' + repr(
                                                self.image_raw.shape[1]) + 'pixel\n'
                                                                           'The detected blob, however, is ' + repr(
                                                current_width) + ' pixels wide and\n'
                                            + repr(current_height) + ' pixels high.\n'
                                                                     'The pixel/mm is set to ' + repr(
                                                self.pixel_per_mm) + '.\n'
                                                                     'This image will not be saved!\n'
                                                                     'In the future please increase the expected size of the animal')
                else:
                    # save the raw image in the preallocated image array.
                    # This will fail if your animal is bigger than 2* the bounding box!
                    self.image_raw[0:current_width, 0:current_height, self.i_tracking] = \
                        thc.CallImageROI(self.current_frame, current_animal).small_image.copy()

                    # save the binary image in the preallocated image array
                    # todo - get rid of this and just save the value for the threshold. Can be reconstructed later if user
                    # wants to view it. Will save a lot of memory.
                    self.image_thresh[0:current_width, 0:current_height, self.i_tracking] = \
                        thc.CallImageROI(current_image_thresholded, current_animal,
                                         sliced_input_image=self.search_boxes[self.i_tracking,
                                                            0::2]).small_image.copy()

                ###################################################################################################
                # the call below shouldn't be necessary because the image is already smoothed! but it might be
                # advantagous in difficult environments - but it's definitely going to cost CPU power!
                # Note: if turned on, get rid of the following function because it would overwrite the image again!
                # self.image_thresh[0:current_width, 0:current_height, i_tracking] = \
                #    binary_fill_holes(thc.CallImageROI(self.current_image_thresholded, current_animal,
                #                       sliced_input_image=search_boxes[i_tracking, 0::2]).small_image).copy()
                # Todo: maybe implement a button so that user can choose?
                ###################################################################################################

                # save the bounding box coordinates. This is necessary to reconstruct the image as only this part of the
                # image is being saved.
                self.bounding_boxes[:, self.i_tracking] = [current_animal.row_min, current_animal.row_max,
                                                           current_animal.col_min, current_animal.col_max].copy()
                # ... and the centroid
                self.centroids[self.i_tracking, :] = current_animal.centroid_row, current_animal.centroid_col

                # identify head and tail
                # find skeleton
                if THINING_FUNCTION == 'thin':  # todo - find a better way and test before publication if necessary!
                    self.image_skel[:, :, self.i_tracking] = morphology.thin(self.image_thresh[:, :, self.i_tracking])
                else:
                    self.image_skel[:, :, self.i_tracking] = morphology.skeletonize(
                        self.image_thresh[:, :, self.i_tracking])

                # how many points are there in the skeleton (also gives me the length)
                self.length_skeleton[self.i_tracking] = len(np.nonzero(self.image_skel[:, :, self.i_tracking])[0])

                # todo: This is very explicit - change?
                skeleton_y = np.nonzero(self.image_skel[:, :, self.i_tracking])[0]
                skeleton_x = np.nonzero(self.image_skel[:, :, self.i_tracking])[1]
                # this loop will give me an array, which is organized like the current_skeleton, and tells me how many
                # points each point is connected to +1 in all 4 directions possible. Taken from original SOS.
                connect = np.zeros(self.length_skeleton[self.i_tracking])
                for i_skel in range(self.length_skeleton[self.i_tracking]):
                    connect[i_skel] = np.sum(np.logical_and(skeleton_x >= skeleton_x[i_skel] - 1,
                                                            np.logical_and(skeleton_x <= skeleton_x[i_skel] + 1,
                                                                           np.logical_and(
                                                                               skeleton_y >= skeleton_y[i_skel] - 1,
                                                                               skeleton_y <= skeleton_y[i_skel] + 1))))
                skeleton_end_points = np.where(connect == 2)[0]

                aspect_ratio = current_animal.major_axis / current_animal.minor_axis

                if len(skeleton_end_points) == 2:
                    self.endpoints[0, :, self.i_tracking] = current_animal.row_min + skeleton_y[skeleton_end_points[0]], \
                                                            current_animal.col_min + skeleton_x[skeleton_end_points[0]]
                    self.endpoints[1, :, self.i_tracking] = current_animal.row_min + skeleton_y[skeleton_end_points[1]], \
                                                            current_animal.col_min + skeleton_x[skeleton_end_points[1]]


                    # rules to be able to get to assign head and tail to an image:
                    # 1): aspect ratio (major axis / minor axis of the animal must be higher than a given value
                    # 2): We need exactly two endpoints on the skeleton (when one has holes in the binary image it can become a
                    # a circular skeleton
                    # 3): We need a minimum length of the skeleton.
                    if aspect_ratio > 1.25 and len(skeleton_end_points) == 2 and \
                            self.length_skeleton[self.i_tracking] > 1 / \
                            2 * np.nanmean(self.length_skeleton[self.i_tracking - 3:self.i_tracking]):

                        # in case we have not yet assigned the head, at the beginning of the experiment
                        # which endpoint has the shortest distance to the centroid of the original larva = tail
                        if self.i_tracking == self.start_frame:
                            # if self.tails[self.i_tracking-1,0] == 0:
                            if np.linalg.norm(
                                    self.first_centroid - self.endpoints[0, :, self.i_tracking]) < np.linalg.norm(
                                    self.first_centroid - self.endpoints[1, :,
                                                          self.i_tracking]):  # endpoint 0 is the tail
                                self.tails[self.i_tracking, :] = self.endpoints[0, :, self.i_tracking]
                                self.heads[self.i_tracking, :] = self.endpoints[1, :, self.i_tracking]
                            else:
                                self.tails[self.i_tracking, :] = self.endpoints[1, :, self.i_tracking]
                                self.heads[self.i_tracking, :] = self.endpoints[0, :, self.i_tracking]

                        # todo - had to introduce the and len(Self.to_fix) > 0 because in an adult fly experiment
                        # where the fly was hard to see it wouldn't append to_fix and throw an error here
                        elif self.tails[self.i_tracking - 1, 0] == 0 and len(self.to_fix) > 0:
                            # This happens when you have a donut shaped animal in the frames before.
                            # idea: The last verified centroid should always be closer to the tail than the head, which
                            # can move much more
                            if np.linalg.norm(
                                    self.endpoints[0, :, self.i_tracking] - self.centroids[self.to_fix[0] - 1, :]) < \
                                    np.linalg.norm(
                                        self.endpoints[1, :, self.i_tracking] - self.centroids[self.to_fix[0] - 1, :]):
                                self.tails[self.i_tracking, :] = self.endpoints[0, :, self.i_tracking]
                                self.heads[self.i_tracking, :] = self.endpoints[1, :, self.i_tracking]
                            else:
                                self.tails[self.i_tracking, :] = self.endpoints[1, :, self.i_tracking]
                                self.heads[self.i_tracking, :] = self.endpoints[0, :, self.i_tracking]

                            # we'll also fix, in retrospect, the lost self.heads and self.tails with the centroid coordinate
                            # to easily be able to plot afterwards
                            self.tails[self.to_fix, :] = self.centroids[self.to_fix, :].copy()
                            self.heads[self.to_fix, :] = self.centroids[self.to_fix, :].copy()

                            self.to_fix = []
                        else:
                            if np.linalg.norm(
                                    self.tails[self.i_tracking - 1, :] - self.endpoints[0, :,
                                                                         self.i_tracking]) < np.linalg.norm(
                                self.tails[self.i_tracking - 1, :] - self.endpoints[1, :, self.i_tracking]):
                                self.tails[self.i_tracking, :] = self.endpoints[0, :, self.i_tracking]
                                self.heads[self.i_tracking, :] = self.endpoints[1, :, self.i_tracking]
                            else:
                                self.tails[self.i_tracking, :] = self.endpoints[1, :, self.i_tracking]
                                self.heads[self.i_tracking, :] = self.endpoints[0, :, self.i_tracking]

                        # the if clause below checks if the tail is in front of the the centroid, essentially checking if
                        # there's a potential head/tail swap in the assignment.

                        # First if just checks if there have been enough frames collected already to compare with the
                        # current frame.
                        if self.i_tracking > self.recording_framerate:
                            # Then we calculate the distance of the centroid between the past 1 second and the
                            # current frame. This will be needed to calculate speed. Note - even though the centroid is the
                            # most stable point, it can be very noisy!
                            distance_centroid = np.linalg.norm(
                                self.centroids[self.i_tracking - self.recording_framerate, :] - self.centroids[
                                                                                                self.i_tracking, :])
                            # print('Distance in pixels: ' + repr(distance_centroid))
                            # print('Distance in mm: ' + repr(distance_centroid/self.pixel_per_mm))

                            # next there is a filter for the minimum speed of the centroid needs to have in order to consider
                            # the animal 'moving'.
                            # speed mm per sec = distance centroid / px_per_mm  / framerate
                            current_centroid_speed = (distance_centroid / self.pixel_per_mm) / self.recording_framerate
                            if self.tails[self.i_tracking - 1, 0] != 0 and self.tails[self.i_tracking, 0] != 0 and \
                                    current_centroid_speed > 0.25:
                                # We'll check if a tail has been assigned or if the curvature of the animal was too great.
                                # then it checks if the distance travelled was enough to justify looking for a H/T swap

                                # this first checks in which direction the centroid is traveling relative to the frame before:
                                # theta_centroid = centroid _current - centroid_past
                                direction_centroid = np.arctan2(self.centroids[self.i_tracking, 0] -
                                                                self.centroids[
                                                                    self.i_tracking - self.frames_to_define_orientation, 0],
                                                                self.centroids[self.i_tracking, 1] -
                                                                self.centroids[
                                                                    self.i_tracking - self.frames_to_define_orientation, 1])
                                # if the centroid is in front of the tail, we'll always get a similar angle as the
                                # direction of centroid if the trail is in front of the centroid (H/T sway) we should get
                                # the opposite angle theta_tail = centroid_current - tail_current
                                direction_tail_minus_centroid = np.arctan2(self.centroids[self.i_tracking, 0] -
                                                                           self.tails[self.i_tracking, 0],
                                                                           self.centroids[self.i_tracking, 1] -
                                                                           self.tails[self.i_tracking, 1])
                                # next we'll normalize, i.e. we bring the movement of the centroid onto the horizonal axis
                                # and let the direction of the tail relative to the centroid follow
                                if direction_centroid - direction_tail_minus_centroid < -np.pi or direction_centroid - \
                                        direction_tail_minus_centroid > np.pi:
                                    normalized_angle = direction_centroid + direction_tail_minus_centroid
                                else:
                                    normalized_angle = direction_centroid - direction_tail_minus_centroid

                                # now we check if the normalized angle is bigger or smaller than given freedom we
                                # give it. 1/2*pi is half a circle worth of freedom of movement.
                                if normalized_angle > 1 / 2 * np.pi or normalized_angle < - 1 / 2 * np.pi:
                                    print('found HT swap frame ' + repr(self.i_tracking))
                                    # print('movement: ' + repr(direction_centroid))
                                    # print('tail dir: ' + repr(direction_tail_minus_centroid))
                                    self.ht_swap[self.i_tracking] = 1

                                    # todo - let user decide. Why not fix them? To see what exactly was used to stimulate, extremity
                                    # or centroid.
                                    if self.repair_ht_swaps:

                                        # In order to be robust against noise the heat tail swap needs to persistent for
                                        # at least one second - Todo Results for framerate 1 may be problematic!
                                        if (self.ht_swap[
                                            self.i_tracking - self.recording_framerate:self.i_tracking] == 1).all():
                                            print('now I could repair the ht swaps')
                                            tails_temp = np.zeros((self.recording_framerate, 2), dtype=np.int16);
                                            heads_temp = np.zeros((self.recording_framerate, 2), dtype=np.int16)

                                            np.copyto(tails_temp,
                                                      self.heads[
                                                      int(
                                                          self.i_tracking - self.recording_framerate + 1):self.i_tracking + 1,
                                                      :])
                                            np.copyto(heads_temp,
                                                      self.tails[
                                                      int(
                                                          self.i_tracking - self.recording_framerate + 1):self.i_tracking + 1,
                                                      :])

                                            np.copyto(
                                                self.tails[
                                                int(self.i_tracking - self.recording_framerate + 1):self.i_tracking + 1,
                                                :],
                                                tails_temp)
                                            np.copyto(self.heads[int(
                                                self.i_tracking - self.recording_framerate + 1):self.i_tracking + 1, :],
                                                      heads_temp)

                                            self.ht_swap[
                                            self.i_tracking - self.recording_framerate + 1:self.i_tracking + 1] = 0

                    else:
                        # need to assign the centroid position to the head/tail - otherwise in the VR setting we have
                        # huge jumps! This means that temporarily tails and heads == centroids if nothing can be assigned
                        self.tails[self.i_tracking, :] = self.centroids[self.i_tracking, :]
                        self.heads[self.i_tracking, :] = self.centroids[self.i_tracking, :]
                        self.to_fix.append(self.i_tracking)

                self.heuristic_parameters[0, self.i_tracking] = current_animal.filled_area
                self.heuristic_parameters[1, self.i_tracking] = current_animal.filled_area / self.pixel_per_mm
                self.heuristic_parameters[2, self.i_tracking] = current_animal.major_axis / \
                                                                current_animal.minor_axis
                self.heuristic_parameters[3, self.i_tracking] = current_animal.eccentricity
                self.heuristic_parameters[4, self.i_tracking] = self.length_skeleton[self.i_tracking]
                # this is inefficient, but very explicit!
                self.heuristic_parameters[5, self.i_tracking] = self.heuristic_parameters[4, self.i_tracking] / \
                                                                self.pixel_per_mm

                if self.i_tracking > 0:
                    self.heuristic_parameters[6, self.i_tracking] = np.linalg.norm(self.centroids[self.i_tracking,:] -
                                                                                   self.centroids[self.i_tracking-1,:]) / \
                                                                    self.recording_framerate
                    # this is inefficient, but very explicit!
                    self.heuristic_parameters[7, self.i_tracking] = self.heuristic_parameters[6, self.i_tracking] / \
                                                                    self.pixel_per_mm
                    # this is inefficient, but very explicit!
                    self.heuristic_parameters[8, self.i_tracking] = self.heuristic_parameters[7, self.i_tracking] * \
                                                                    self.recording_framerate

                # The plotting part
                if self.debug_mode:

                    # display the amount of frames left
                    self.time_remaining.configure(text='Frames remaining: ' + repr(number_of_frames_left - self.i_tracking))


                    bounding_box_raw = smoothed_current_local_image.copy()
                    bounding_box_raw = resize(bounding_box_raw, ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                        self.i_tracking, 1]) * (self.resize),
                                                                  abs(self.search_boxes[self.i_tracking, 2] - self.search_boxes[
                                                                      self.i_tracking, 3]) * (self.resize))),
                                              preserve_range=True, mode='reflect')

                    self.photo_raw = ImageTk.PhotoImage(image=Image.fromarray(bounding_box_raw))
                    self.child_canvas_top_left.create_image(0, 0, image=self.photo_raw, anchor=tk.NW)

                    subtracted_image = current_image_thresholded.astype(np.uint8).copy()
                    subtracted_image[np.where(subtracted_image == 1)] = 255
                    subtracted_image = resize(subtracted_image, ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                        self.i_tracking, 1]) * (self.resize), abs(self.search_boxes[self.i_tracking, 2] -
                                                             self.search_boxes[self.i_tracking, 3]) *
                                                                  (self.resize))),
                                              preserve_range=True, mode='reflect')

                    self.photo_subtracted = ImageTk.PhotoImage(image=Image.fromarray(subtracted_image))
                    self.child_canvas_top_middle.create_image(0, 0, image=self.photo_subtracted, anchor=tk.NW)

                    detected_raw_box = smoothed_current_local_image.copy()
                    try:
                        # draw top horizontal line
                        rr, cc = line(int(self.bounding_boxes[0, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[2, self.i_tracking] - self.search_boxes[self.i_tracking, 2]),
                                      int(self.bounding_boxes[0, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[3, self.i_tracking] - self.search_boxes[self.i_tracking, 2]))
                        detected_raw_box[rr, cc] = self.box_intensity
                        # draw right vertical line
                        rr, cc = line(int(self.bounding_boxes[0, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[3, self.i_tracking] - self.search_boxes[self.i_tracking, 2]),
                                      int(self.bounding_boxes[1, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[3, self.i_tracking] - self.search_boxes[self.i_tracking, 2]))
                        detected_raw_box[rr, cc] = self.box_intensity
                        # draw bottom horizontal line
                        rr, cc = line(int(self.bounding_boxes[1, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[2, self.i_tracking] - self.search_boxes[self.i_tracking, 2]),
                                      int(self.bounding_boxes[1, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[3, self.i_tracking] - self.search_boxes[self.i_tracking, 2]))
                        detected_raw_box[rr, cc] = self.box_intensity
                        # draw left vertical line
                        rr, cc = line(int(self.bounding_boxes[0, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[2, self.i_tracking] - self.search_boxes[self.i_tracking, 2]),
                                      int(self.bounding_boxes[1, self.i_tracking] - self.search_boxes[self.i_tracking, 0]),
                                      int(self.bounding_boxes[2, self.i_tracking] - self.search_boxes[self.i_tracking, 2]))
                        detected_raw_box[rr, cc] = self.box_intensity

                        detected_raw_box = resize(detected_raw_box,
                                                  ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                                                      self.i_tracking, 1]) * (self.resize),
                                                    abs(self.search_boxes[self.i_tracking, 2] - self.search_boxes[
                                                        self.i_tracking, 3]) * (self.resize))),
                                                  preserve_range=True, mode='reflect')

                        self.photo_raw_detected = ImageTk.PhotoImage(image=Image.fromarray(detected_raw_box))
                        self.child_canvas_top_right.create_image(0, 0, image=self.photo_raw_detected, anchor=tk.NW)
                    except:
                        pass

                    self.child.update()


                    # full frame
                    # image_to_plot = current_frame.copy() # todo copy not necessary?
                    # image_to_plot[self.tails[self.i_tracking, 0], self.tails[i_tracking, 1]] = 0
                    # image_to_plot[self.centroids[i_tracking, 0], self.centroids[i_tracking, 1]] = 50

                elif self.preview == 'overview':

                    self.size_of_scatterplot = 1

                    # print the remaining frames
                    self.time_remaining.configure(text='Frames remaining: ' + repr(number_of_frames_left - self.i_tracking))
                    # HIGHLY inefficient - currently the scatterplot that only updates a maximum of three points
                    # per frame has to be completely redrawn. Better: Define the image ones, then set data and just
                    # update the figure
                    if self.i_tracking == 0:
                        if self.vr_arena is not None:
                            scatterplot = self.vr_arena
                            print('took scatterplot as vr_arena')
                        else:
                            scatterplot = np.zeros((int(round(self.current_frame.shape[0]/ self.size_of_scatterplot)),
                                                    int(round(self.current_frame.shape[1] / self.size_of_scatterplot))),
                                                   np.uint16)

                        scatterplot[int(round(self.centroids[self.i_tracking, 0])),
                                    int(round(self.centroids[self.i_tracking, 1]))] = 65535
                        scatterplot_object = self.ax_vr_arena.imshow(scatterplot)
                    else:
                        scatterplot[int(round(self.centroids[self.i_tracking, 0])),
                                    int(round(self.centroids[self.i_tracking, 1]))] = 65535

                        scatterplot_object.set_array(scatterplot)
                        print('centroid x: ' + repr(self.centroids[self.i_tracking, 1]) + ' centroid y: ' + repr(
                            self.centroids[self.i_tracking, 0]))
                    self.canvas.draw_idle()
                    # self.photo_current_frame = ImageTk.PhotoImage(image=Image.fromarray(current_frame))
                    # self.photo_current_frame = ImageTk.PhotoImage(image=Image.fromarray(scatterplot))
                    # self.child_canvas.create_image(0, 0, image=self.photo_current_frame, anchor=tk.NW)
                    # self.child_canvas.update_idletasks()

                    local_image = self.image_raw[:, :, self.i_tracking]
                    local_image = resize(local_image,
                                         ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                                             self.i_tracking, 1]) * self.small_image_resize,
                                           abs(self.search_boxes[self.i_tracking, 2] - self.search_boxes[
                                               self.i_tracking, 3]) * self.small_image_resize)),
                                         preserve_range=True, mode='reflect')

                    self.photo_local = ImageTk.PhotoImage(image=Image.fromarray(local_image))
                    self.child_local_canvas.create_image(0, 0, image=self.photo_local, anchor=tk.NW)

                    converted_binary_image = self.image_thresh[:, :, self.i_tracking].astype(np.uint8)
                    converted_binary_image[np.where(converted_binary_image == 1)] = 255
                    converted_binary_image = resize(converted_binary_image,
                                                    ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                                                        self.i_tracking, 1]) * self.small_image_resize,
                                                      abs(self.search_boxes[self.i_tracking, 2] - self.search_boxes[
                                                          self.i_tracking, 3]) * self.small_image_resize)),
                                                    preserve_range=True, mode='reflect')
                    self.photo_binary = ImageTk.PhotoImage(image=Image.fromarray(converted_binary_image))
                    self.child_binary_canvas.create_image(0, 0, image=self.photo_binary, anchor=tk.NW)

                    converted_skeleton_image = self.image_skel[:, :, self.i_tracking].astype(np.uint8)
                    converted_skeleton_image[np.where(converted_skeleton_image == 1)] = 255
                    converted_skeleton_image = resize(converted_skeleton_image,
                                                      ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                                                          self.i_tracking, 1]) * self.small_image_resize,
                                                        abs(self.search_boxes[self.i_tracking, 2] - self.search_boxes[
                                                            self.i_tracking, 3]) * self.small_image_resize)),
                                                      preserve_range=True, mode='reflect')
                    self.photo_skeleton = ImageTk.PhotoImage(image=Image.fromarray(converted_skeleton_image))
                    self.child_skeleton_canvas.create_image(0, 0, image=self.photo_skeleton, anchor=tk.NW)

                elif self.preview == 'only animal':

                    # writing the frames that are left that left
                    self.time_remaining.configure(text='Frames remaining: ' + repr(number_of_frames_left - self.i_tracking))

                    # The image that should be diplayed
                    local_image = smoothed_current_local_image
                    # rescale it to the size requested
                    local_image = resize(local_image,
                                         ((abs(self.search_boxes[self.i_tracking, 0] - self.search_boxes[
                                             self.i_tracking, 1]) * self.small_image_resize,
                                           abs(self.search_boxes[self.i_tracking, 2] - self.search_boxes[
                                               self.i_tracking, 3]) * self.small_image_resize,)),
                                         preserve_range=True, mode='reflect').astype(np.uint8)

                    # Turn the greysale image into a "color" picture by adding another color
                    local_image = np.repeat(local_image[..., None], 3, axis=2)
                    # Find the centroid in the small, resized image and turn to high intensity in channel 0
                    local_image[(self.centroids[self.i_tracking, 0] - self.search_boxes[self.i_tracking, 0]) *
                                self.small_image_resize - self.small_image_resize:
                                (self.centroids[self.i_tracking, 0] - self.search_boxes[self.i_tracking, 0]) *
                                self.small_image_resize + self.small_image_resize,
                    (self.centroids[self.i_tracking, 1] - self.search_boxes[self.i_tracking, 2]) *
                    self.small_image_resize - self.small_image_resize:
                    (self.centroids[self.i_tracking, 1] - self.search_boxes[self.i_tracking, 2]) *
                    self.small_image_resize + self.small_image_resize,
                    0] = 255

                    #if self.Drosophila_larva:
                    # Find the identified head in the small, resized image and turn to high intensity in channel 1
                    if self.heads[self.i_tracking, 0] != 0:
                        local_image[(self.heads[self.i_tracking, 0] - self.search_boxes[self.i_tracking, 0]) *
                                    self.small_image_resize - self.small_image_resize:(self.heads[self.i_tracking, 0] -
                                                                                       self.search_boxes[
                                                                                           self.i_tracking, 0]) * self.small_image_resize +
                                                                                      self.small_image_resize,
                        (self.heads[self.i_tracking, 1] - self.search_boxes[self.i_tracking, 2]) *
                        self.small_image_resize - self.small_image_resize:(self.heads[self.i_tracking, 1] -
                                                                           self.search_boxes[
                                                                               self.i_tracking, 2]) * self.small_image_resize + self.small_image_resize,
                        1] = 255

                    self.photo_local = ImageTk.PhotoImage(image=Image.fromarray(local_image))
                    self.child_canvas.create_image(0, 0, image=self.photo_local, anchor=tk.NW)

                else:
                    # writing the time that is left
                    self.time_remaining.configure(text='Frames remaining: ' + repr(number_of_frames_left - self.i_tracking))

                self.child.update()

                if self.i_tracking > 0:
                    actual_time = time.time() - start_time
                    print('Frame: ' + repr(self.i_tracking) + ' Loop takes: ' + repr(actual_time)[
                                                                           0:5] + 's Interval requested: '
                          + repr(self.interval / 1000)[0:5] + 's')

                    if actual_time < self.interval / 1000:
                        # pause for the time difference. The accuracy of this depends on the OS - generally it is said that
                        # Windows machines have a precision of about 10-15ms while linux machines should be in the single
                        # digits ms - Raspberry Pi runs on linux.
                        self.child.after(int(round(self.interval - actual_time * 1000)))
                    else:
                        # If the user asks for more frames per second than can be delivered, the programme skips the pause
                        # and notifies the user
                        print('Frame: ' + repr(self.i_tracking) + ' Took: ' + repr(actual_time)[0:5] + 's')
                        print('You asked for more frames per second that can be delivered')

                """
                # When the user is able to do an offline anaylsis a number had to be assigned for the fastest possible
                # analysis. I believe the number 10'000 to be sufficent for this for now!
                if self.display_framerate <= 10000:
    
                    if i_tracking >= 1:
                        # calculate the time spent since last being after this if clause. This includes both the time it took
                        # to plot the previous figure + the time it took to run this instance of the loop (data collection and
                        # analysis)
    
                        actual_time = time.time() - self.real_time[i_tracking - 1]
                        print('Frame: ' + repr(i_tracking) + ' Loop takes: ' + repr(actual_time)[
                                                                               0:5] + 's Interval requested: '
                              + repr(self.interval / 1000)[0:5] + 's')
                        # We then compare the value between the actual time spent doing all that and the interval the user
                        # asked for. If it's smaller (meaning the computer is fast enough to fullfill the wish) we have to pause.
                        if actual_time < self.interval / 1000:
                            # pause for the time difference. The accuracy of this depends on the OS - generally it is said that
                            # Windows machines have a precision of about 10-15ms while linux machines should be in the single
                            # digits ms - Raspberry Pi runs on linux.
                            if self.preview != 'none':
                                # one shouldn't mix e.g. time.sleep with a tkinter function!
                                self.child.after(int(round(self.interval - actual_time * 1000)))
    
                            else:
                                self.child.after(int(round(self.interval - actual_time * 1000)))
                                # time.sleep(self.interval / 1000 - actual_time)
                        else:
                            # If the user asks for more frames per second than can be delivered, the programme skips the pause
                            # and notifies the user
                            if self.run_loop:
                                print('Frame: ' + repr(i_tracking) + ' Took: ' + repr(actual_time)[0:5] + 's')
                                print('You asked for more frames per second that can be delivered')
                    # in case the experiment is being conducted, record the real time
                    if Raspberry:
                        self.real_time[i_tracking] = time.time()
                    # if the experiment is analyzed afterwards, there is no way of knowing what the real time was, so it is
                    # just assumed that the camera was perfect and gave exactly the framerate that was requested
                    elif not Raspberry or self.offline_analysis:
                        self.real_time[i_tracking] = time.time()
                        self.display_time[i_tracking] = i_tracking / self.recording_framerate
                else:
                    self.display_time[i_tracking] = i_tracking / self.recording_framerate
                """
        #except:
        #    print('Stopped tracking unexpected at frame ' + repr(i_tracking + 1) + '. Did it leave the arena?')

        print('done tracking - saving')

        self.child.after(0, lambda: thc.Save(heads=self.heads,
                                              tails=self.tails,
                                              centroids=self.centroids,
                                              image_skel=self.image_skel,
                                              image_raw=self.image_raw,
                                              image_thresh=self.image_thresh,
                                              background=self.mean_image,
                                              real_time=self.timestamps[:,0],
                                              pixel_per_mm=self.pixel_per_mm,
                                              bounding_boxes=self.bounding_boxes,
                                             stimulation = self.stimulation,
                                             heuristic_data=self.heuristic_parameters))

        self.child.after(300, lambda: self.child.destroy())
        print('done saving')